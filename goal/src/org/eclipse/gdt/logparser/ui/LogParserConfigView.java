package org.eclipse.gdt.logparser.ui;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.eclipse.gdt.Activator;
import org.eclipse.gdt.logparser.LogParserThread;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.ProgressIndicator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.part.ViewPart;

import goal.preferences.LoggingPreferences;


public class LogParserConfigView extends ViewPart {

	Action selectAllAction;
	
	public final static String VIEW_ID = "org.eclipse.gdt.debug.ui.LogParserConfig";

	private static Composite container;
	
	private ProgressIndicator progress;
	
	private static String program;
	
	public static String logFile;
		
	public LogParserConfigView() {
		super();
		program = System.getProperty("user.home") + "\\Documents\\Goal\\GOALLogAnalyser.exe";
	}
	
	@Override
	public void createPartControl(Composite parent) {
		if(programExists()) {
		
			logFile = Activator.getDefault().getPreferenceStore().getString(LoggingPreferences.Pref.logdirectory.name());
			System.out.println("current file is: " + logFile);	
			
			container = new Composite(parent, SWT.NONE);
			GridLayout grid = new GridLayout();
			grid.numColumns = 2;
			grid.verticalSpacing = 15;
			container.setLayout(grid);
				
			// Items in setting tab
			Text text = new Text(container, SWT.MULTI | SWT.READ_ONLY | SWT.WRAP);
			text.setEditable(false);
			text.setText("Current log file: ");
			text.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			
			Text logFolder = new Text(container, SWT.MULTI | SWT.READ_ONLY | SWT.WRAP);
			logFolder.setEditable(false);
			logFolder.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			logFolder.setText("test");
			
			// Set the correct text for the configuration panel
			if(folderExists()) {
				logFolder.setText(logFile);
			} else {
				logFolder.setText("Please use the button below to select a log folder");
			}
			
			
			Button run = new Button(container, SWT.PUSH);
			run.setBounds(0, 10, 75, 25);
			run.setText("Analyze logs");
			run.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					if(folderExists()) {
						Display.getDefault().asyncExec(new Runnable() {
							@Override
							public void run() {
								// TODO Auto-generated method stub
								progress.beginAnimatedTask();
							}
						});
						
						startParser();
					} else {
						MessageBox message = new MessageBox(new Shell(), SWT.ERROR_CANNOT_GET_ITEM);
						message.setText("Error finding log folder");
						message.setMessage("We were unable to find the folder you selected");
						message.open();
					}
				}					
			});
			
			
			// Let the user select a different folder for logging
			Button button = new Button(container, SWT.PUSH);
			button.setBounds(0,10,75,25);
			button.setText("Select log folder");
			button.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					DirectoryDialog dialog = new DirectoryDialog(new Shell(), SWT.NULL);
					dialog.setText("Use");
					
					// If the specified folder does not exist ask the user for a different one.
					File folder = new File(logFile.replace('\\', '/'));
					if (folder.isDirectory()) {
						dialog.setFilterPath(folder.getAbsolutePath());
					}
					String newFile = dialog.open();
					if(newFile != null) {
						logFile = newFile;
						logFolder.setText(logFile);
					}
				}
			});
			
			progress = new ProgressIndicator(container);
			
		}
	}

	@Override
	public void setFocus() {
		container.setFocus();		
	}

	private void startParser() {
		Display.getDefault().asyncExec(new LogParserThread(logFile, program, progress));
	}
	
	private boolean folderExists() {
		File folder = new File(logFile.replace("\\", "/"));
		System.out.println(folder.exists());
		return (folder.isDirectory() || folder.exists());
	}
	
	private boolean programExists() {
		File file = new File(program);
		if(!file.exists()) {
			MessageBox message = new MessageBox(new Shell(), SWT.ERROR_CANNOT_GET_ITEM);
			message.setText("Cannot find the parser");
			message.setMessage("Please make sure you download and install the GoalLogAnalyser from https://github.com/CptWesley/GOALLogAnalyser/releases/latest \n" +
			"Also make sure that you install this application in the Documents\\Goal folder.");
			message.open();
			return false;
		} else {
			System.out.println("Found file!");
			return true;
		}
	}
}
