package org.eclipse.gdt.logparser.ui;

import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

public class GOALPerspectiveFactory implements IPerspectiveFactory{

	private static final String VIEW_ID = "org.eclipse.gdt.logparser.ui.perspective";
	
	private static final String BOTTOM = "bottom";
	private static final String TOP = "top";
	
	@Override
	public void createInitialLayout(IPageLayout myLayout) {
		
		String OS = System.getProperty("os.name");
		if (!OS.equals("Windows")) {
			MessageBox error = new MessageBox(new Shell());
			error.setText("Fault in operating system");
			error.setMessage("Goal log parser does not support: " + OS + "operating system \n Please use windows!");
		} else {
		
			// TODO Auto-generated method stub
			myLayout.addShowViewShortcut(LogParserConfigView.VIEW_ID);
			myLayout.addShowViewShortcut(LogParserAnalysisView.VIEW_ID);
			
			
			IFolderLayout analytics = myLayout.createFolder(TOP, 
					IPageLayout.TOP, 0.6f, myLayout.getEditorArea());
			analytics.addView(LogParserAnalysisView.VIEW_ID);
			analytics.addView(VIEW_ID);
			
			IFolderLayout bot = myLayout.createFolder(BOTTOM, IPageLayout.BOTTOM, 0.40f, 
									myLayout.getEditorArea());
			bot.addView(LogParserConfigView.VIEW_ID);
			bot.addView(VIEW_ID);
		}
	}
}
