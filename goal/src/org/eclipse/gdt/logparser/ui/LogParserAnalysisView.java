package org.eclipse.gdt.logparser.ui;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;

public class LogParserAnalysisView extends ViewPart{

	public static final String VIEW_ID = "org.eclipse.gdt.debug.ui.LogParserAnalysis";
	
	private static Composite container;
	
	public LogParserAnalysisView() {
		super();
	}
	
	@Override
	public void createPartControl(Composite parent) {
		container = new Composite(parent, SWT.NONE);
		container.setLayout(new FillLayout(SWT.VERTICAL));
		
		Browser browser = new Browser(container, SWT.NONE);
		browser.setBounds(0,0,600,400);
		browser.setUrl("C:\\Users\\cedri\\Documents\\Goal\\index.html");
		
		
		WatchService watcher = null;
		WatchKey key;
		try {
			watcher = FileSystems.getDefault().newWatchService();
			Path dir = Paths.get("C:\\Users\\cedri\\Documents\\Goal\\");
			dir.register(watcher, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_MODIFY);
			
			while(true) {
				key = watcher.take();
				
				for (WatchEvent<?> event : key.pollEvents()) {
			        // get event type
			        WatchEvent.Kind<?> kind = event.kind();
			 
			        // get file name
			        @SuppressWarnings("unchecked")
			        WatchEvent<Path> ev = (WatchEvent<Path>) event;
			        Path fileName = ev.context();
			 
			        System.out.println(kind.name() + ": " + fileName);
			 
			        if(kind == StandardWatchEventKinds.ENTRY_MODIFY) {
			        	browser.refresh();
			        }
				}
			}
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
				
	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub
		container.setFocus();
	}

}
