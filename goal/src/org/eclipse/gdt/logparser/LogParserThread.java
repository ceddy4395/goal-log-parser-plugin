package org.eclipse.gdt.logparser;

import java.io.IOException;

import org.eclipse.jface.dialogs.ProgressIndicator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

public class LogParserThread implements Runnable{

	private String location;
	private String application;
	private ProgressIndicator progress;
	
	public LogParserThread(String file, String application, ProgressIndicator progress) {
		location = "-logs=" + file;
		this.application = application;
		this.progress = progress;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		
		try {
			Process process = new ProcessBuilder(application,"-site", location).start();
		} catch (IOException e) {
			MessageBox errorMessage = new MessageBox(new Shell(), SWT.ICON_ERROR | SWT.OK);
			errorMessage.setText("Error starting analysis");
			errorMessage.setMessage(e.getMessage());
			errorMessage.open();
			e.printStackTrace();
		}
		progress.dispose();		
	}

}
