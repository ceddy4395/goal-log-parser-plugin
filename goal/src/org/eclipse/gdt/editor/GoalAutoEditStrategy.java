package org.eclipse.gdt.editor;

import org.eclipse.dltk.ui.text.util.AutoEditUtils;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.DefaultIndentLineAutoEditStrategy;
import org.eclipse.jface.text.DocumentCommand;
import org.eclipse.jface.text.IDocument;

/**
 * Auto indent strategy sensitive to brackets.
 */
public class GoalAutoEditStrategy extends DefaultIndentLineAutoEditStrategy {

	@Override
	public void customizeDocumentCommand(final IDocument d, final DocumentCommand c) {
		if (c.length == 0 && c.text != null && endsWithDelimiter(d, c.text)) {
			smartIndentAfterNewLine(d, c);
		} else if ("}".equals(c.text)) {
			smartInsertAfterBracket(d, c);
		}
		autoClose(d, c);
	}

	private boolean endsWithDelimiter(final IDocument d, final String txt) {
		final String[] delimiters = d.getLegalLineDelimiters();
		for (final String delimiter : delimiters) {
			if (txt.endsWith(delimiter)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Set the indent of a new line based on the command provided in the
	 * supplied document.
	 *
	 * @param document
	 *            - the document being parsed
	 * @param command
	 *            - the command being performed
	 */
	protected void smartIndentAfterNewLine(final IDocument document, final DocumentCommand command) {
		final int docLength = document.getLength();
		if (command.offset == -1 || docLength == 0) {
			return;
		}
		try {
			final int p = (command.offset == docLength ? command.offset - 1 : command.offset);
			final int line = document.getLineOfOffset(p);
			final StringBuffer buf = new StringBuffer(command.text);
			if (command.offset < docLength && document.getChar(command.offset) == '}') {
				int indLine = findMatchingOpenBracket(document, line, command.offset, 0);
				if (indLine == -1) {
					indLine = line;
				}
				buf.append(getIndentOfLine(document, indLine));
			} else {
				final int start = document.getLineOffset(line);
				final int whiteend = findEndOfWhiteSpace(document, start, command.offset);
				buf.append(document.get(start, whiteend - start));
				if (getBracketCount(document, start, command.offset, true) > 0) {
					buf.append('\t');
				}
			}
			command.text = buf.toString();
		} catch (final Exception ignore) {
		}
	}

	/**
	 * Set the indent of a bracket based on the command provided in the supplied
	 * document.
	 *
	 * @param document
	 *            - the document being parsed
	 * @param command
	 *            - the command being performed
	 */
	protected void smartInsertAfterBracket(final IDocument document, final DocumentCommand command) {
		if (command.offset == -1 || document.getLength() == 0) {
			return;
		}
		try {
			final int p = (command.offset == document.getLength() ? command.offset - 1 : command.offset);
			final int line = document.getLineOfOffset(p);
			final int start = document.getLineOffset(line);
			final int whiteend = findEndOfWhiteSpace(document, start, command.offset);
			if (whiteend == command.offset) {
				final int indLine = findMatchingOpenBracket(document, line, command.offset, 1);
				if (indLine != -1 && indLine != line) {
					final StringBuffer replaceText = new StringBuffer(getIndentOfLine(document, indLine));
					replaceText.append(document.get(whiteend, command.offset - whiteend));
					replaceText.append(command.text);
					command.length = command.offset - start;
					command.offset = start;
					command.text = replaceText.toString();
				}
			}
		} catch (final Exception ignore) {
		}
	}

	/**
	 * Returns the line number of the next bracket after end.
	 *
	 * @returns the line number of the next matching bracket after end
	 * @param document
	 *            - the document being parsed
	 * @param line
	 *            - the line to start searching back from
	 * @param end
	 *            - the end position to search back from
	 * @param closingBracketIncrease
	 *            - the number of brackets to skip
	 * @return The position of the matching open bracket
	 * @throws BadLocationException
	 */
	protected int findMatchingOpenBracket(final IDocument document, int line, int end, final int closingBracketIncrease)
			throws BadLocationException {
		int start = document.getLineOffset(line);
		int brackcount = getBracketCount(document, start, end, false) - closingBracketIncrease;
		while (brackcount < 0) {
			line--;
			if (line < 0) {
				return -1;
			}
			start = document.getLineOffset(line);
			end = start + document.getLineLength(line) - 1;
			brackcount += getBracketCount(document, start, end, false);
		}
		return line;
	}

	/**
	 * Returns the bracket value of a section of text. Closing brackets have a
	 * value of -1 and open brackets have a value of 1.
	 *
	 * @returns the line number of the next matching bracket after end
	 * @param document
	 *            - the document being parsed
	 * @param start
	 *            - the start position for the search
	 * @param end
	 *            - the end position for the search
	 * @param ignoreCloseBrackets
	 *            - whether or not to ignore closing brackets in the count
	 * @return The bracket value
	 * @throws BadLocationException
	 *             if the document position is invalid
	 */
	private int getBracketCount(final IDocument document, final int start, final int end, boolean ignoreCloseBrackets)
			throws BadLocationException {
		int begin = start;
		int bracketcount = 0;
		while (begin < end) {
			final char curr = document.getChar(begin);
			begin++;
			switch (curr) {
			case '/':
				if (begin < end) {
					final char next = document.getChar(begin);
					if (next == '*') {
						begin = getCommentEnd(document, begin + 1, end);
					} else if (next == '/') {
						begin = end;
					}
				}
				break;
			case '*':
				if (begin < end) {
					final char next = document.getChar(begin);
					if (next == '/') {
						bracketcount = 0;
						begin++;
					}
				}
				break;
			case '{':
				bracketcount++;
				ignoreCloseBrackets = false;
				break;
			case '}':
				if (!ignoreCloseBrackets) {
					bracketcount--;
				}
				break;
			case '"':
			case '\'':
				begin = getStringEnd(document, begin, end, curr);
				break;
			default:
			}
		}
		return bracketcount;
	}

	/**
	 * Returns the end position a comment starting at pos.
	 *
	 * @returns the end position a comment starting at pos
	 * @param document
	 *            - the document being parsed
	 * @param position
	 *            - the start position for the search
	 * @param end
	 *            - the end position for the search
	 * @return the end of a comment
	 * @throws BadLocationException
	 *             if the document position is invalid
	 */
	private int getCommentEnd(final IDocument document, final int position, final int end) throws BadLocationException {
		int currentPosition = position;
		while (currentPosition < end) {
			final char curr = document.getChar(currentPosition);
			currentPosition++;
			if (curr == '*') {
				if (currentPosition < end && document.getChar(currentPosition) == '/') {
					return currentPosition + 1;
				}
			}
		}
		return end;
	}

	/**
	 * Returns the String at line with the leading whitespace removed.
	 *
	 * @returns the String at line with the leading whitespace removed.
	 * @param document
	 *            - the document being parsed
	 * @param line
	 *            - the line being searched
	 * @return an indentation string for the line
	 * @throws BadLocationException
	 *             if the document position is invalid
	 */
	protected String getIndentOfLine(final IDocument document, final int line) throws BadLocationException {
		if (line > -1) {
			final int start = document.getLineOffset(line);
			final int end = start + document.getLineLength(line) - 1;
			final int whiteend = findEndOfWhiteSpace(document, start, end);
			return document.get(start, whiteend - start);
		} else {
			return "";
		}
	}

	/**
	 * Returns the position of the character in the document after position.
	 *
	 * @returns the next location of character.
	 * @param document
	 *            - the document being parsed
	 * @param position
	 *            - the position to start searching from
	 * @param end
	 *            - the end of the document
	 * @param character
	 *            - the character you are trying to match
	 * @return the end of the string
	 * @throws BadLocationException
	 *             if the document position is invalid
	 */
	private int getStringEnd(final IDocument document, final int position, final int end, final char character)
			throws BadLocationException {
		int currentPosition = position;
		while (currentPosition < end) {
			final char currentCharacter = document.getChar(currentPosition);
			currentPosition++;
			if (currentCharacter == '\\') {
				currentPosition++;
			} else if (currentCharacter == character) {
				return currentPosition;
			}
		}
		return end;
	}

	private void autoClose(final IDocument d, final DocumentCommand c) {
		if (c.offset == -1) {
			return;
		}
		try {
			final char current = c.text.charAt(0);
			switch (current) {
			case '\"':
			case '\'':
				if ('\"' == current && c.offset > 0 && "\"".equals(d.get(c.offset - 1, 1))) {
					return;
				} else if ('\'' == current && c.offset > 0 && "\'".equals(d.get(c.offset - 1, 1))) {
					return;
				} else if (c.offset != d.getLength() && c.text.charAt(0) == d.get(c.offset, 1).charAt(0)) {
					c.text = "";
				} else {
					c.text += c.text;
				}
				c.shiftsCaret = false;
				c.caretOffset = c.offset + 1;
				break;
			case '(':
				// case '{':
			case '[':
				if (!(c.offset != d.getLength() && current == d.get(c.offset, 1).charAt(0))) {
					c.shiftsCaret = false;
					c.text = c.text + AutoEditUtils.getBracePair(c.text.charAt(0));
					c.caretOffset = c.offset + 1;
				}
				break;
			// case '}':
			case ']':
			case ')':
				if (c.offset != d.getLength() && current == d.get(c.offset, 1).charAt(0)) {
					c.text = "";
					c.shiftsCaret = false;
					c.caretOffset = c.offset + 1;
					return;
				}
				break;
			}
		} catch (final Exception ignore) {
		}
	}
}