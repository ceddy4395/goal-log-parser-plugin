package org.eclipse.gdt;

import java.io.File;
import java.net.URI;
import java.net.URL;

import org.eclipse.core.resources.IMarkerDelta;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.ICoreRunnable;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.IBreakpointListener;
import org.eclipse.debug.core.model.IBreakpoint;
import org.eclipse.dltk.core.DLTKCore;
import org.eclipse.gdt.debug.GoalLineBreakpoint;
import org.eclipse.gdt.debug.dbgp.DebuggerCollection;
import org.eclipse.gdt.editor.ComboToolbar;
import org.eclipse.gdt.editor.GoalTextTools;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPartListener2;
import org.eclipse.ui.IWorkbenchPartReference;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import goal.preferences.Preferences;
import goal.tools.eclipse.DebugCommand;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin implements IBreakpointListener, IPartListener2 {
	public static final String PLUGIN_ID = "org.eclipse.gdt";
	private static final String agentVersion = "2.0.12";
	private static Activator plugin;
	private String JARpath;
	private String Agentpath;
	private GoalTextTools fGoalTextTools;
	private ComboToolbar toolbar;
	private Job build;

	public Activator() {
	}

	public String getJARpath() {
		return this.JARpath;
	}

	public String getAgentPath() {
		return this.Agentpath;
	}

	@Override
	public void start(final BundleContext context) throws Exception {
		super.start(context);
		plugin = this;

		String JARpathT = "";
		try {
			final URL found1 = DebugCommand.class.getProtectionDomain().getCodeSource().getLocation();
			final String found2 = FileLocator.resolve(found1).toString().replaceAll("\\s", "%20");
			final URI found3 = new URI(found2);
			JARpathT = new File(found3).getCanonicalPath();
		} catch (final Exception e) {
			DLTKCore.error(e);
		}
		this.JARpath = JARpathT;
		String AgentpathT = "";
		try { // Locate required libraries
			final URL found1 = FileLocator.find(getDefault().getBundle(), new Path("lib"), null);
			final String found2 = FileLocator.resolve(found1).toString().replaceAll("\\s", "%20");
			final URI found3 = new URI(found2);
			AgentpathT = new File(found3).getCanonicalPath();
		} catch (final Exception e) {
			DLTKCore.error(e);
		}
		this.Agentpath = AgentpathT + File.separator + "agents-" + agentVersion + "-bin.zip";

		final IPath prefs = ResourcesPlugin.getWorkspace().getRoot().getLocation().append(".goalprefs");
		Preferences.changeSettingsFile(prefs.toFile());

		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				DebugPlugin.getDefault().getBreakpointManager().addBreakpointListener(Activator.this);
				final IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
				window.getActivePage().addPartListener(Activator.this);
				if (window.getPartService().getActivePartReference() != null) {
					partActivated(window.getPartService().getActivePartReference());
				}
			}
		});
	}

	@Override
	public void stop(final BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	public static Activator getDefault() {
		return plugin;
	}

	public GoalTextTools getTextTools() {
		if (this.fGoalTextTools == null) {
			this.fGoalTextTools = new GoalTextTools(true);
		}
		return this.fGoalTextTools;
	}

	public static Image getImage(final String imagePath) {
		final ImageDescriptor imageDescriptor = AbstractUIPlugin.imageDescriptorFromPlugin(Activator.PLUGIN_ID,
				imagePath);
		return imageDescriptor.createImage();
	}

	public void setActiveToolbar(final ComboToolbar toolbar) {
		this.toolbar = toolbar;
	}

	public ComboToolbar getActiveToolbar() {
		return this.toolbar;
	}

	@Override
	public void breakpointAdded(final IBreakpoint ibreakpoint) {
		if (ibreakpoint instanceof GoalLineBreakpoint) {
			final GoalLineBreakpoint breakpoint = (GoalLineBreakpoint) ibreakpoint;
			final DebuggerCollection collection = DebuggerCollection
					.getCollection(breakpoint.getMarker().getResource().getProject().getName());
			if (collection != null && collection.getMainDebugger() != null) {
				collection.getMainDebugger().updateBreakpoints();
			}
		}
	}

	@Override
	public void breakpointRemoved(final IBreakpoint ibreakpoint, final IMarkerDelta odelta) {
		breakpointAdded(ibreakpoint);
	}

	@Override
	public void breakpointChanged(final IBreakpoint obreakpoint, final IMarkerDelta odelta) {
	}

	@Override
	public void partActivated(final IWorkbenchPartReference partRef) {
		if (partRef.getPage().getActiveEditor() != null) {
			try {
				final IEditorInput editor = partRef.getPage().getActiveEditor().getEditorInput();
				final IProject project = (editor == null) ? null : editor.getAdapter(IResource.class).getProject();
				if (project != null && project.isAccessible() && project.hasNature(GoalNature.GOAL_NATURE)) {
					if (Activator.this.toolbar != null) {
						Activator.this.toolbar.resolveMAS(editor);
					}
					if (Activator.this.build != null) {
						Activator.this.build.cancel();
					}
					Activator.this.build = Job.create("Building " + project.getName() + "...", new ICoreRunnable() {
						@Override
						public void run(IProgressMonitor monitor) throws CoreException {
							project.build(IncrementalProjectBuilder.FULL_BUILD, null);
						}
					});
					Activator.this.build.setPriority(Job.BUILD);
					Activator.this.build.schedule();
				}
			} catch (final Exception e) {
				DLTKCore.error(e);
			}
		}
	}

	@Override
	public void partBroughtToTop(final IWorkbenchPartReference partRef) {
	}

	@Override
	public void partClosed(final IWorkbenchPartReference partRef) {
	}

	@Override
	public void partDeactivated(final IWorkbenchPartReference partRef) {
	}

	@Override
	public void partOpened(final IWorkbenchPartReference partRef) {
	}

	@Override
	public void partHidden(final IWorkbenchPartReference partRef) {
	}

	@Override
	public void partVisible(final IWorkbenchPartReference partRef) {
	}

	@Override
	public void partInputChanged(final IWorkbenchPartReference partRef) {
	}
}