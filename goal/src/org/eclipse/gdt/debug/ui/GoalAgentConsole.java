package org.eclipse.gdt.debug.ui;

import org.eclipse.gdt.debug.dbgp.LocalDebugger;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IPerspectiveDescriptor;
import org.eclipse.ui.IPerspectiveListener;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleConstants;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;
import org.eclipse.ui.internal.console.ConsoleView;

import languageTools.program.agent.AgentId;

@SuppressWarnings("restriction")
public class GoalAgentConsole extends MessageConsole {
	private final MessageConsoleStream output;
	private String last;

	public GoalAgentConsole(final AgentId agent) {
		super(agent == null ? "Action history" : agent.getName(), null);
		setWaterMarks(80000, 100000); // FIXME: good defaults?!
		this.output = newMessageStream();
		this.last = "";
	}

	public void println(final String s) {
		this.last = s;
		this.output.println(s);
	}

	public String getLast() {
		return this.last;
	}

	public void initialize(final LocalDebugger parent) {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() { // FIXME: horrendous function
				final IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
				final IPerspectiveDescriptor current = window.getActivePage().getPerspective();
				if (current != null && current.getId().equals("org.eclipse.gdt.debug.perspective")) {
					try {
						final ConsoleView main = ((ConsoleView) window.getActivePage()
								.findView(IConsoleConstants.ID_CONSOLE_VIEW));
						parent.setMainConsole(main);
						window.getActivePage().showView(GoalAgentConsoleView.ID_AGENT_CONSOLE_VIEW,
								GoalAgentConsole.this.getName(), IWorkbenchPage.VIEW_VISIBLE);
						ConsolePlugin.getDefault().getConsoleManager()
								.addConsoles(new IConsole[] { GoalAgentConsole.this });
					} catch (final Exception e) {
						parent.err(e);
					}
				} else {
					window.addPerspectiveListener(new IPerspectiveListener() {
						@Override
						public void perspectiveChanged(final IWorkbenchPage page,
								final IPerspectiveDescriptor perspective, final String changeId) {
							// perspectiveActivated(page, perspective);
						}

						@Override
						public void perspectiveActivated(final IWorkbenchPage page,
								final IPerspectiveDescriptor perspective) {
							if (perspective != null
									&& perspective.getId().equals("org.eclipse.gdt.debug.perspective")) {
								try {
									final ConsoleView main = ((ConsoleView) page
											.findView(IConsoleConstants.ID_CONSOLE_VIEW));
									parent.setMainConsole(main);
									window.removePerspectiveListener(this);
									page.showView(GoalAgentConsoleView.ID_AGENT_CONSOLE_VIEW, getName(),
											IWorkbenchPage.VIEW_VISIBLE);
									ConsolePlugin.getDefault().getConsoleManager()
											.addConsoles(new IConsole[] { GoalAgentConsole.this });
								} catch (final Exception e) {
									parent.err(e);
								}
							}
						}
					});
				}
			}
		});

	}

	@Override
	protected void dispose() {
		super.dispose();
		ConsolePlugin.getDefault().getConsoleManager().removeConsoles(new IConsole[] { this });
	}
}
