package org.eclipse.gdt.debug.ui;

import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.internal.console.ConsoleView;

@SuppressWarnings("restriction")
public class GoalAgentConsoleView extends ConsoleView {
	public final static String ID_AGENT_CONSOLE_VIEW = "org.eclipse.gdt.AgentConsoleView";

	@Override
	public void consolesAdded(final IConsole[] consoles) {
		if (consoles.length == 1 && consoles[0].getName().equals(getViewSite().getSecondaryId())) {
			super.consolesAdded(consoles);
			setPinned(true);
			setPartName(consoles[0].getName());
		}
	}

	@Override
	public void consolesRemoved(final IConsole[] consoles) {
		if (consoles.length == 1 && consoles[0].getName().equals(getViewSite().getSecondaryId())) {
			setPinned(false);
			super.consolesRemoved(consoles);
		}
	}

	@Override
	public void setFocus() {
	}
}