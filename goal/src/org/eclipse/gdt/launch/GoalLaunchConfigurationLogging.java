package org.eclipse.gdt.launch;

import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.ui.AbstractLaunchConfigurationTab;
import org.eclipse.dltk.core.DLTKCore;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import goal.preferences.DBExportPreferences;
import goal.preferences.LoggingPreferences;

public class GoalLaunchConfigurationLogging extends AbstractLaunchConfigurationTab {
	private Button showlogtime;
	private Button logtofile;
	private Button overwritelogfiles;
	private Button logconsoles;
	private Button stackdump;
	private Button exportbeliefs;
	private Button exportpercepts;
	private Button exportmailbox;
	private Button exportgoals;
	private Button separatefiles;
	private Button openaftersave;

	@Override
	public void createControl(final Composite parent) {
		final Composite comp = new Composite(parent, SWT.NONE);
		setControl(comp);
		final GridData data = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		data.horizontalSpan = 2;
		comp.setLayout(new GridLayout(data.horizontalSpan, true));
		comp.setFont(parent.getFont());

		this.showlogtime = createCheckButtonInGrid(comp, "Show log time", data);
		this.logtofile = createCheckButtonInGrid(comp, "Write logs to files", data);
		this.overwritelogfiles = createCheckButtonInGrid(comp, "Overwrite old log files", data);
		this.logconsoles = createCheckButtonInGrid(comp, "Log the default consoles to file", data);
		this.stackdump = createCheckButtonInGrid(comp, "Add Java stack traces to errors/warnings", data);

		createVerticalSpacer(comp, data.horizontalSpan);

		this.exportbeliefs = createCheckButtonInGrid(comp, "Export beliefbase", data);
		this.exportpercepts = createCheckButtonInGrid(comp, "Include percepts", data);
		this.exportmailbox = createCheckButtonInGrid(comp, "Include mails", data);
		this.exportgoals = createCheckButtonInGrid(comp, "Export goalbase", data);
		this.separatefiles = createCheckButtonInGrid(comp, "Export to separate files", data);
		this.openaftersave = createCheckButtonInGrid(comp, "Open export after save", data);
	}

	private Button createCheckButtonInGrid(final Composite parent, final String label, final GridData data) {
		final Button returned = createCheckButton(parent, label);
		returned.setLayoutData(data);
		returned.setFont(parent.getFont());
		returned.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent e) {
				updateLaunchConfigurationDialog();
			}
		});
		return returned;
	}

	@Override
	public void setDefaults(final ILaunchConfigurationWorkingCopy configuration) {
		configuration.setAttribute(LoggingPreferences.Pref.showlogtime.name(), false);
		configuration.setAttribute(LoggingPreferences.Pref.logtofile.name(), false);
		configuration.setAttribute(LoggingPreferences.Pref.overwritelogfiles.name(), false);
		configuration.setAttribute(LoggingPreferences.Pref.logconsoles.name(), false);
		configuration.setAttribute(LoggingPreferences.Pref.stackdump.name(), false);

		configuration.setAttribute(DBExportPreferences.Pref.exportbeliefs.name(), true);
		configuration.setAttribute(DBExportPreferences.Pref.exportpercepts.name(), true);
		configuration.setAttribute(DBExportPreferences.Pref.exportmailbox.name(), true);
		configuration.setAttribute(DBExportPreferences.Pref.exportgoals.name(), true);
		configuration.setAttribute(DBExportPreferences.Pref.separatefiles.name(), true);
		configuration.setAttribute(DBExportPreferences.Pref.openaftersave.name(), false);
	}

	@Override
	public void initializeFrom(final ILaunchConfiguration configuration) {
		try {
			this.showlogtime
					.setSelection(configuration.getAttribute(LoggingPreferences.Pref.showlogtime.name(), false));
			this.logtofile.setSelection(configuration.getAttribute(LoggingPreferences.Pref.logtofile.name(), false));
			this.overwritelogfiles
					.setSelection(configuration.getAttribute(LoggingPreferences.Pref.overwritelogfiles.name(), false));
			this.logconsoles
					.setSelection(configuration.getAttribute(LoggingPreferences.Pref.logconsoles.name(), false));
			this.stackdump.setSelection(configuration.getAttribute(LoggingPreferences.Pref.stackdump.name(), false));

			this.exportbeliefs
					.setSelection(configuration.getAttribute(DBExportPreferences.Pref.exportbeliefs.name(), true));
			this.exportpercepts
					.setSelection(configuration.getAttribute(DBExportPreferences.Pref.exportpercepts.name(), true));
			this.exportmailbox
					.setSelection(configuration.getAttribute(DBExportPreferences.Pref.exportmailbox.name(), true));
			this.exportgoals
					.setSelection(configuration.getAttribute(DBExportPreferences.Pref.exportgoals.name(), true));
			this.separatefiles
					.setSelection(configuration.getAttribute(DBExportPreferences.Pref.separatefiles.name(), true));
			this.openaftersave
					.setSelection(configuration.getAttribute(DBExportPreferences.Pref.openaftersave.name(), false));
		} catch (final Exception e) {
			DLTKCore.error(e);
		}
	}

	@Override
	public void performApply(final ILaunchConfigurationWorkingCopy configuration) {
		configuration.setAttribute(LoggingPreferences.Pref.showlogtime.name(), this.showlogtime.getSelection());
		configuration.setAttribute(LoggingPreferences.Pref.logtofile.name(), this.logtofile.getSelection());
		configuration.setAttribute(LoggingPreferences.Pref.overwritelogfiles.name(),
				this.overwritelogfiles.getSelection());
		configuration.setAttribute(LoggingPreferences.Pref.logconsoles.name(), this.logconsoles.getSelection());
		configuration.setAttribute(LoggingPreferences.Pref.stackdump.name(), this.stackdump.getSelection());

		configuration.setAttribute(DBExportPreferences.Pref.exportbeliefs.name(), this.exportbeliefs.getSelection());
		configuration.setAttribute(DBExportPreferences.Pref.exportpercepts.name(), this.exportpercepts.getSelection());
		configuration.setAttribute(DBExportPreferences.Pref.exportmailbox.name(), this.exportmailbox.getSelection());
		configuration.setAttribute(DBExportPreferences.Pref.exportgoals.name(), this.exportgoals.getSelection());
		configuration.setAttribute(DBExportPreferences.Pref.separatefiles.name(), this.separatefiles.getSelection());
		configuration.setAttribute(DBExportPreferences.Pref.openaftersave.name(), this.openaftersave.getSelection());
	}

	@Override
	public String getName() {
		return "Logging";
	}

	@Override
	public String getId() {
		return "org.eclipse.gdt.loggingtab";
	}
}
